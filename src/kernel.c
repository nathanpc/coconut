/**
 * kernel.c
 * Coconut kernel.
 *
 * @author Nathan Campos
 */

#include <stdint.h>
#include <stdbool.h>

#include "io.h"
#include "kstdio.h"

char * itoa( int value, char * str, int base )
{
    char * rc;
    char * ptr;
    char * low;
    // Check for supported base.
    if ( base < 2 || base > 36 )
    {
        *str = '\0';
        return str;
    }
    rc = ptr = str;
    // Set '-' for negative decimals.
    if ( value < 0 && base == 10 )
    {
        *ptr++ = '-';
    }
    // Remember where the numbers start.
    low = ptr;
    // The actual conversion.
    do
    {
        // Modulo is negative for negative value. This trick makes abs() unnecessary.
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz"[35 + value % base];
        value /= base;
    } while ( value );
    // Terminating the string.
    *ptr-- = '\0';
    // Invert the numbers.
    while ( low < ptr )
    {
        char tmp = *low;
        *low++ = *ptr;
        *ptr-- = tmp;
    }
    return rc;
}

void kmain(void) {
	extern uint32_t magic;

	if (magic != 0x2BADB002) {
		// Something went not according to specs. Print an error
		// message and halt, but do *not* rely on the multiboot
		// data structure.
	}

	// Clear the screen.
	clear_screen(true);

	// Print the init messages.
	kprint(0x02, "Coconut");
	kprint(0x0f, " OS v0.1\n\n");

	uint32_t key;
	uint32_t old_key;

	while (true) {
		char lowercase[] = "##1234567890-*##qwertyuiop[]";

		old_key = key;
		key = inb(0x60);

		if (old_key != key) {
			// Checks if the *NORMAL* key is just a released message (no special support)
			if (key < 0x81 && key != 0x1C) { // 1C == Enter
				kputc(0x0f, lowercase[key]);
			} else if (key > 0xD8) {
				// The special chars.
			}
		} else {
			//write_char(' ', 0x0f, 10, 10);
		}
	}/*
	while (true) {
		uint32_t c = inb(0x60);
		char lowercase[] = "##1234567890-*##qwertyuiop[]";

		if (!(c & 80)) {// don't print break codes
			//kputc(0x0f, 'C');
			//kputc(0x0f, lowercase[c]);
			//write_char(lowercase[c], 0x0f, 10, 10);
			write_char('C', 0x0f, 10, 10);
			old_key = c;
		}
	}*/
}
