/**
 * kstdio.h
 *
 * @author Nathan Campos
 */

#ifndef KSTDIO_H_
#define KSTDIO_H_

#include <stdbool.h>

// Defines the screen properties.
typedef struct {
    unsigned int width;
    unsigned int height;

    unsigned int row;
    unsigned int col;

    char buffer[25][81];
} scr;

void write_char(unsigned char c, uint16_t color, int x, int y);

void kputc(uint16_t color, unsigned char c);
void kprint(uint16_t color, const char *string);

void clear_screen(bool clr_buffer);

#endif
