/**
 * kstdio.c
 * Basic stdio stuff for the kernel.
 *
 * @author Nathan Campos
 */

#include <stdint.h>
#include <stdbool.h>

#include "io.h"
#include "kstdio.h"

void update_cursor(unsigned short row, unsigned short col);
void newline();

// Construct the screen struct.
scr screen = {
	.width = 80,
	.height = 25,
	.row = 0,
	.col = 0
};

void write_char(unsigned char c, uint16_t color, int x, int y) {
	// Put the char in the buffer.
	screen.buffer[y][x] = c;

	// Print.
	volatile uint16_t *video = (volatile uint16_t*)0xB8000 + (y * 80 + x);
	*video = c | (color << 8);

	// Update the cursor position.
	update_cursor(y, x + 1);
}

void kputc(uint16_t color, unsigned char c) {
	// FIXME: Adding a lot of chars in a loop makes this crash.
	write_char(c, color, screen.col++, screen.row);
}

void kprint(uint16_t color, const char *string) {
	// Iterate through the string until \0 (termination char)
	while (*string != '\0') {
		// TODO: Check if screen.col == 80 and add a newline.
		if (*string == '\n') {
			//write_char('\0', 0x90, screen.col, screen.row);  // Debug if \n.
			newline();

			// Jump the unused char.
			*string++;
		} else {
			write_char(*string++, color, screen.col++, screen.row);
		}
	}
}

void update_cursor(unsigned short row, unsigned short col) {
	unsigned short position = (row * 80) + col;

	// Cursor LOW port to vga INDEX register
	outb(0x3D4, 0x0F);
	outb(0x3D5, (unsigned char)(position & 0xFF));

	// Cursor HIGH port to vga INDEX register.
	outb(0x3D4, 0x0E);
	outb(0x3D5, (unsigned char)((position >> 8) & 0xFF));
}

void clear_screen(bool clr_buffer) {
	// FIXME: Check why using screen.(h/w) for y and x limit doesn't work here.
	for (uint8_t y = 0; y < 25; y++) {
		for (uint8_t x = 0; x < 80; x++) {
			volatile uint16_t *video = (volatile uint16_t*)0xB8000 + (y * 80 + x);
			*video = ' ' | (0x00 << 8);

			if (clr_buffer) {
				screen.buffer[y][x] = '\0';
			}
		}
	}
}

void newline() {
	if (screen.row >= 24) {
		// Clear the screen.
		clear_screen(false);

		for (uint8_t i = 1; i < 25; i++) {
			screen.col = 0;
			screen.row = i - 1;

			kprint(0x0f, screen.buffer[i]);
		}

		// FIXME: Needs more testing.
		// Just for the ++ to adjust and put the cursor in the correct place.
	}

	screen.col = 0;
	screen.row++;
	update_cursor(screen.row, screen.col);
}
