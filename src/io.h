/**
 * io.h
 *
 * @author Nathan Campos
 */

#ifndef IO_H_
#define IO_H_

unsigned char inb(unsigned short port);
void outb(unsigned short port, unsigned char val);

#endif
