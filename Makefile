CC = i586-elf-gcc
CFLAGS = -std=gnu99 -Wall -Wextra -nostdlib -fno-builtin -nostartfiles -nodefaultlibs
LD = i586-elf-ld

SRCDIR=src
OBJFILES = $(SRCDIR)/loader.o $(SRCDIR)/io.o $(SRCDIR)/kstdio.o $(SRCDIR)/kernel.o
#OBJFILES += $(SRCDIR)/stdlib/snprintf.o
 
all: kernel.img
 
.s.o:
	@echo "Building Assembly files"
	nasm -f elf -o $@ $<
	@echo ""
 
.c.o:
	@echo "Building C files"
	$(CC) $(CFLAGS) -o $@ -c $<
	@echo ""
 
kernel.bin: $(OBJFILES)
	@echo "Linking everything together"
	$(LD) -T linker.ld -o $@ $^
	@echo ""
 
kernel.img: kernel.bin
	@echo "Creating the disk image"
	dd if=/dev/zero of=pad bs=1 count=750
	cat grub/stage1 grub/stage2 grub/pad $< > $@
 
clean:
	@echo "Cleaning the mess"
	$(RM) $(OBJFILES) kernel.bin kernel.img
 
distclean:
	$(RM) $(OBJFILES) kernel.bin
